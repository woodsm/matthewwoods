---
title: Woods Engineering
date: '2020-09-27'
description: My client portfolio and how I came about creating Woods Engineering.
---

<!-- @format -->

To start I would like to say how thankful I am for my partners and their trust in me early in my career. I've been offering custom applications (desktop/web) for more than three years as Woods Engineering. It's been a huge passion of mine and hopefully one day is expands into something much larger than me.

You can find all my work on GitHub at [mxttwoods](https://github.com/mxttwoods)

[All Year Heating and Cooling](https://allyearheating.com)

[DJ Markiee Freshh](https://djmark.info)

[Dream Girl Fishing](https://dreamgirlfishing.com)

Feel free to contact me at: [matthew@woods.engineering](mailto:matthew@woods.engineering)
