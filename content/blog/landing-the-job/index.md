---
title: Landing the Job
date: '2020-09-26'
description: Setting realistic expectations when entering the I.T. job market.
---

<!-- @format -->

Wow! I love blogging so much already.

I want to use this post to speak on getting a job in I.T.

First of all, it's not easy! But you can do it!!
I think people online never speak of how long or how hard it was for them to get to where they are today.

You can get a job without a degree! Use real-world projects and certifications in popular areas to showcase yourself and someone will notice!

Personally, it took me two years of programming and three years into a degree to get my first programming job. Before that I was working as a Telecom Contractor. Long story short don't be afraid to take the support/help desk/end-user job in the end you will grow and learn things that will make you more valuable as a programmer and employee!

I highly recommend making a [linkedin](https://linkedin.com) and using its network to look for opportunity.

I also recommend joining an open source community and start contributing to a project that means something to you. This is a great way to build real world projects that will showcase your skills all while contributing to something meaningful.

For instance I'm a donor to the [#a11y](https://www.a11yproject.com/) project and several others that enable this awesome programming community.
