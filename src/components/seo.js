/**
 *  eslint-disable no-mixed-spaces-and-tabs
 *  eslint-disable import/no-extraneous-dependencies
 *  eslint-disable react/jsx-filename-extension
 *  eslint-disable no-tabs
 * @format
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Helmet } from 'react-helmet'
import { useStaticQuery, graphql } from 'gatsby'

const SEO = ({ description, lang, meta, title }) => {
 const { site } = useStaticQuery(
  graphql`
   query {
    site {
     siteMetadata {
      title
      description
      social {
       email
      }
     }
    }
   }
  `
 )

 const metaDescription = description || site.siteMetadata.description

 return (
  <Helmet
   htmlAttributes={{
    lang
   }}
   title={title}
   titleTemplate={`%s | ${site.siteMetadata.title}`}
   meta={[
    {
     name: 'description',
     content: metaDescription
    },
    {
     property: 'og:title',
     content: title
    },
    {
     property: 'og:description',
     content: metaDescription
    },
    {
     property: 'og:type',
     content: 'website'
    },
    {
     name: 'twitter:card',
     content: 'This is my personal website and blog.'
    },
    {
     name: 'twitter:creator',
     content: site.siteMetadata.social.twitter
    },
    {
     name: 'twitter:title',
     content: title
    },
    {
     name: 'twitter:description',
     content: metaDescription
    }
   ].concat(meta)}
  />
 )
}

SEO.defaultProps = {
 lang: 'en',
 meta: [],
 description: 'This is my personal website and blog.'
}

SEO.propTypes = {
 description: PropTypes.string,
 lang: PropTypes.string,
 meta: PropTypes.arrayOf(PropTypes.object),
 title: PropTypes.string.isRequired
}

export default SEO
