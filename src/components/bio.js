/**
 *  eslint-disable react/jsx-filename-extension
 *  eslint-disable no-mixed-spaces-and-tabs
 *  eslint-disable no-tabs
 *  See: https://www.gatsbyjs.org/docs/use-static-query/
 * @format
 */

import React from 'react'
import Image from 'gatsby-image'
import { useStaticQuery, graphql } from 'gatsby'
import { rhythm } from '../utils/typography'

const Bio = () => {
 const data = useStaticQuery(graphql`
  query BioQuery {
   avatar: file(absolutePath: { regex: "/profile-pic.jpg/" }) {
    childImageSharp {
     fixed(width: 50, height: 50) {
      ...GatsbyImageSharpFixed
     }
    }
   }
   site {
    siteMetadata {
     author {
      name
      summary
     }
     social {
      email
     }
    }
   }
  }
 `)

 const { author, social } = data.site.siteMetadata
 return (
  <div
   style={{
    display: 'flex',
    marginBottom: rhythm(2.5)
   }}
  >
   <Image
    fixed={data.avatar.childImageSharp.fixed}
    alt={author.name}
    style={{
     marginRight: rhythm(1 / 2),
     marginBottom: 0,
     minWidth: 50,
     borderRadius: '100%'
    }}
    imgStyle={{
     borderRadius: '50%'
    }}
   />
   <p>
    {' '}
    Written by{' '}
    <a href={social.email}>
     <strong>
      {author.name}
      <br />
     </strong>
    </a>{' '}
    {author.summary}
   </p>
  </div>
 )
}

export default Bio
