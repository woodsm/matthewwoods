<!-- @format -->

# Personal Site (matthew.codes)

[![CircleCI](https://circleci.com/gh/circleci/circleci-docs.svg?style=svg)](https://circleci.com/gh/circleci/circleci-docs)
![Build and Deploy](https://github.com/mxttwoods/matthewwoods/workflows/Build%20and%20Deploy/badge.svg)
[![CodeFactor](https://www.codefactor.io/repository/github/mxttwoods/matthewwoods/badge)](https://www.codefactor.io/repository/github/mxttwoods/matthewwoods)
![GitHub contributors](https://img.shields.io/github/contributors/mxttwoods/matthewwoods)
![GitHub issues](https://img.shields.io/github/issues/mxttwoods/matthewwoods)
![GitHub pull requests](https://img.shields.io/github/issues-pr/mxttwoods/matthewwoods)
![GitHub release (latest by date)](https://img.shields.io/github/v/release/mxttwoods/matthewwoods)
![GitHub Release Date](https://img.shields.io/github/release-date/mxttwoods/matthewwoods)
![GitHub repo size](https://img.shields.io/github/repo-size/mxttwoods/matthewwoods)
![GitHub code size in bytes](https://img.shields.io/github/languages/code-size/mxttwoods/matthewwoods)
![GitHub file size in bytes](https://img.shields.io/github/size/mxttwoods/matthewwoods/src/pages/index.js)
![GitHub last commit](https://img.shields.io/github/last-commit/mxttwoods/matthewwoods)
![GitHub language count](https://img.shields.io/github/languages/count/mxttwoods/matthewwoods)
![GitHub top language](https://img.shields.io/github/languages/top/mxttwoods/matthewwoods)
![GitHub](https://img.shields.io/github/license/mxttwoods/matthewwoods)
![Website](https://img.shields.io/website?url=https%3A%2F%2Fmatthew.codes)

## Summary

This is a pretty simple jamstack blog made with gatsby. I mainly use this dump my thoughts and use as the home for Woods Engineering my dba.

## Authors

- **Matthew Woods** - _Developer_ - [Matthew](https://github.com/mxttwoods)

## License

This project is licensed under the MIT License - see the LICENSE.md file for details
