module.exports = {
 siteMetadata: {
  title: 'Matthew Codes',
  author: {
   name: 'Matthew Woods',
   summary: 'UNCG - I.S.M. Alumni & Software Developer'
  },
  description: 'A blog',
  siteUrl: 'https://matthew.codes',
  social: {
   email: 'mailto:matthew@woods.engineering',
   twitter: 'mxttwoods'
  }
 },
 plugins: [
  {
   resolve: 'gatsby-source-filesystem',
   options: {
    path: `${__dirname}/content/blog`,
    name: 'blog'
   }
  },
  {
   resolve: 'gatsby-source-filesystem',
   options: {
    path: `${__dirname}/content/assets`,
    name: 'assets'
   }
  },
  {
   resolve: 'gatsby-transformer-remark',
   options: {
    plugins: [
     {
      resolve: 'gatsby-remark-images',
      options: {
       maxWidth: 590
      }
     },
     {
      resolve: 'gatsby-remark-responsive-iframe',
      options: {
       wrapperStyle: 'margin-bottom: 1.0725rem'
      }
     },
     'gatsby-remark-prismjs',
     'gatsby-remark-copy-linked-files',
     'gatsby-remark-smartypants'
    ]
   }
  },
  'gatsby-transformer-sharp',
  'gatsby-plugin-sharp',
  {
   resolve: 'gatsby-plugin-google-analytics',
   options: {
    trackingId: 'UA-242564002'
   }
  },
  'gatsby-plugin-feed',
  {
   resolve: 'gatsby-plugin-manifest',
   options: {
    name: "Matthew's Blog",
    short_name: "Matt's Blog",
    start_url: '/',
    background_color: '#ffffff',
    theme_color: '#663399',
    display: 'minimal-ui',
    icon: 'content/assets/gatsby-icon.png'
   }
  },
  'gatsby-plugin-react-helmet',
  {
   resolve: 'gatsby-plugin-typography',
   options: {
    pathToConfigModule: 'src/utils/typography'
   }
  },
  `gatsby-plugin-preact`,
  `gatsby-plugin-offline`,
  {
   resolve: 'gatsby-plugin-firebase',
   options: {
    credentials: {
     apiKey: 'AIzaSyC0PvzrkFpglrN0VVs3Rra-CfplfirXu0w',
     authDomain: 'mw-web-project.firebaseapp.com',
     databaseURL: 'https://mw-web-project.firebaseio.com',
     projectId: 'mw-web-project',
     storageBucket: 'mw-web-project.appspot.com',
     messagingSenderId: '338330438517',
     appId: '1:338330438517:web:f837a787b76f46a42e213c',
     measurementId: 'G-4Q81DWGL3B'
    }
   }
  }
 ]
}
