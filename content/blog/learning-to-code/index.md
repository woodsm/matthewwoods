---
title: Learning to Code
date: '2020-09-25'
description: Making the first steps into the world of computer science and programming.
---

<!-- @format -->

This is my first post on my new blog, oh wow how exciting!

I hope to start this blog off by sharing some of the programming resources I use on the job and during my personal time. These are not the \*best resources by any means, and this isn't meant to be a course. This article should serve more as an introduction and not curriculum. If you are looking for a course or curriculum to get your feet wet I highly recommend starting with [Codecademy.com](https://www.codecademy.com/learn/paths/computer-science)

I'd like to speak on my background in computer science; I started programming in C# at UNCG and it was a great learning experience. Then I shifted into mostly Java 8 and Javascript at my first programming job as senior. Now post-grad I work as a contractor for a telecommunications provider and my current team has a focus on python and the web.

Start Here (from scratch)

<https://code.visualstudio.com/docs/getstarted/introvideos>

<https://www.codecademy.com/learn/learn-java>

<https://www.freecodecamp.org/>

<https://www.codecademy.com/learn/learn-python>

<https://www.w3schools.com/>

Keep Learning!!! (more technical)

<https://codelabs.developers.google.com/>

<https://andreasbm.github.io/web-skills/>

<https://github.com/ForrestKnight/open-source-cs>

<https://developer.mozilla.org/en-US/>

<https://web.dev/learn/>

Learn the www ... (javascript)

<https://github.com/microsoft/TypeScript-Node-Starter>

<https://github.com/h5bp/html5-boilerplate>

<https://github.com/mdjsn/references>

<https://github.com/florinpop17/app-ideas>

<https://github.com/andrews1022/web-development-2020-course-list>

<http://todomvc.com/>

<https://www.w3schools.com/js/js_es6.asp>

<https://www.digitalocean.com/community/tutorials>

Stay inspired!!

<https://github.com/NationalSecurityAgency>

<https://github.com/The-Art-of-Hacking/h4cker>

Get certified and get hired:

<https://aws.amazon.com/training/learning-paths/?nav=tc&loc=4>

<https://education.oracle.com/oracle-certification-path/pFamily_48>

I'm currently learning:

<https://pytorch.org/>

<https://www.tensorflow.org/>

<https://angular.io/>

<https://nextjs.org/>

<https://svelte.dev/>

<https://codelabs.developers.google.com/codelabs/cloud-run-django/index.html?index=..%2F..index#0>

<https://aws.amazon.com/certification/certified-cloud-practitioner/>

<https://developers.google.com/edu/c++/>
